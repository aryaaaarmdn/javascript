// Manipulasi Elemen
//     Beberapa method :
//         element.innerHTML = untuk mengubah isi dari tag
//         element.style.property

//         element.setAttribute()
//         element.classList


// Manipulasi menggunakan Inner HTML
// const judul = document.getElementById('judul');
// judul.innerHTML = "<em>Teks Ini diganti dengan js</em>";

// const sectionA = document.querySelector('section#a');
// sectionA.innerHTML = "Hello World";
// sectionA.innerHTML = '<div><p>Paragraf</p></div>';




// Manipulasi style css
// const judul = document.querySelector('#judul');
// judul.style.color = 'lightblue';
// judul.style.backgroundColor = 'Salmon';



// Manipulasi atribut
const judul = document.getElementsByTagName('h1')[0];
judul.setAttribute('name', 'arya');

const a = document.querySelector('section#a a');
console.log(a.getAttribute('href')); // mengetahui isi dari atribut
console.log(judul.getAttribute('id')); // mengetahui isi dari atribut

a.removeAttribute('href'); // menghapus atribut 

// const p2 = document.querySelector('.p2');
// p2.setAttribute('class', 'label'); // akan menghapus class sebelumnya yang sudah dibuat (menimpa)





// classList = untuk mengelola class
// add() = untuk menambah class baru
const p2 = document.querySelector('.p2');
p2.classList.add('label');
p2.classList.add('satu');
p2.classList.add('dua');
p2.classList.add('tiga');

// remove() = untuk menghapus class tertentu
p2.classList.remove('label');

// toggle() = menambahkan ketika class belum ada dan akan menghapus ketika class sudah ada
p2.classList.toggle('label');

// item() = untuk mengetahui class tertentu
console.log(p2.classList.item(2));


// contains() = untuk mengecek apakah mempunyai class tertentu
console.log(p2.classList.contains('dua')); // true
console.log(p2.classList.contains('empat')); // false


// replace() = mengganti class yang ada dengan class yang baru
p2.classList.replace('tiga', 'empat'); // mengubah class tiga menjadi empat