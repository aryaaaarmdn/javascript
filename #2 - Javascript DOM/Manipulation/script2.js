// Manipulasi Node
// Beberapa method dari js :


// seleksi element parent
const sectionA = document.getElementById('a');
const sectionB = document.getElementById('b');
const ul = document.querySelector('section#b ul');
const li2 = ul.querySelector('li:nth-child(2)');

// 1. document.createElement()
const pBaru = document.createElement('p'); // bikin tag paragraf baru
const liBaru = document.createElement('li'); // bikin tag list baru
const h2 = document.createElement('h2'); // bikin tag h2 (node baru untuk di replace nanti)

// 2. document.createTextNode()
const teksPBaru = document.createTextNode('Paragraf Baru'); // membuat teks baru untuk paragraf nanti
const teksLiBaru = document.createTextNode('List Baru'); // membuat teks baru untuk tag li nanti
const teksH2 = document.createTextNode('Heading 2 Baru Menggunakan js'); // membuat teks baru untuk h2 nanti

// 3. node.appendChild()
pBaru.appendChild(teksPBaru); // memasukkan isi teks paragraf baru ke element paragraf 
sectionA.appendChild(pBaru); // memasukkan elemen p (termasuk isi teks) ke dalam section A
liBaru.appendChild(teksLiBaru); // memasukkan teks kedalam tag li baru
h2.appendChild(teksH2); // memasukkan teks kedalam tag h2

// 4. node.insertBefore()
ul.insertBefore(liBaru, li2); // parent.insertBefore(node baru, elemen setelah node baru)

// 5. parentNode.removeChild()
const a = document.querySelector('a');
sectionA.removeChild(a);

// 6. parentNode.replaceChild()
const paragraf = sectionB.querySelector('p'); // tangkap node yang akan di ganti (node lama)
sectionB.replaceChild(h2, paragraf); // mengganti node yang lama dengan yang baru; sintak : parent.replaceChild(node baru, node lama)


// elemen yang dimanipulasi melalui DOM
pBaru.style.backgroundColor = 'lightgreen';
liBaru.style.backgroundColor = 'lightgreen';
h2.style.backgroundColor = 'lightgreen';

// untuk menangkap text pada sebuah node, gunakan method document.textContent