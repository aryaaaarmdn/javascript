// Pilihan Computer
function getPilihanComputer() {
    // Membangkitkan bilangan random untuk computer
    const comp = Math.random();

    if (comp < 0.34) return 'Gajah';
    if (comp > 0.34 && comp < 0.67) return 'Orang';
    return 'Semut';
}

// Hasil
function getHasil(comp, player) {

    if (player == comp) return 'SERI!!';
    if (player == 'Gajah') return (comp == 'Orang') ? 'Menang' : 'Kalah';
    if (player == 'Semut') return (comp == 'Gajah') ? 'Menang' : 'Kalah';
    if (player == 'Orang') return (comp == 'Semut') ? 'Menang' : 'Kalah';
    return 'Tidak Valid';
}



// Elemen gambar comp
const imgComp = document.querySelector('.img-komputer');

// Ambil elemen area info
const tempatHasil = document.querySelector('.info')

// Tangkap elemen pilihan player
const pPlayer = document.querySelectorAll('li img');


function putar() {
    const gambar = ['Gajah', 'Semut', 'Orang'];
    let i = 0;
    const waktuMulai = new Date().getTime();

    setInterval(function () {

        if (new Date().getTime() - waktuMulai > 1000) {
            clearInterval;
            return;
        }

        imgComp.setAttribute('src', 'img/' + gambar[i++] + '.png')
        if (i == gambar.length) i = 0;
    }, 100) // 0,1 detik
}



// Looping img (cara kedua)
pPlayer.forEach(function (pilihan) {
    pilihan.addEventListener('click', function () {

        // Ambil value pilihan computer
        const pComp = getPilihanComputer();

        // Ambil value pilihan player
        const pPlayer = pilihan.className;

        // Hasil
        const hasil = getHasil(pComp, pPlayer);

        // Panggil fungsi putar (Selama 1 detik)
        putar();


        // Jalankan perintah ketika fungsi putar selesai (delay)
        setTimeout(function () {
            // ganti img comp
            imgComp.setAttribute('src', 'img/' + pComp + '.png');

            // Tampilkan hasil di area info
            tempatHasil.innerHTML = hasil;
        }, 1000);

    });
});


// Cara pertama
// Pilihan Player
// Gajah
// const pGajah = document.querySelector('.Gajah');
// pGajah.addEventListener('click', function () {
//     // Ambil value pilihan computer
//     const pComp = getPilihanComputer();

//     // Ambil value pilihan player
//     const pPlayer = pGajah.className;

//     // ganti img comp
//     imgComp.setAttribute('src', 'img/' + pComp + '.png');

//     // Hasil
//     const hasil = getHasil(pComp, pPlayer);

//     // Tampilkan hasil di area info
//     tempatHasil.innerHTML = hasil;

// });

// // Orang
// const pOrang = document.querySelector('.Orang');
// pOrang.addEventListener('click', function () {
//     // Ambil value pilihan computer
//     const pComp = getPilihanComputer();

//     // Ambil value pilihan player
//     const pPlayer = pOrang.className;

//     // ganti img comp
//     imgComp.setAttribute('src', 'img/' + pComp + '.png');

//     // Hasil
//     const hasil = getHasil(pComp, pPlayer);

//     // Tampilkan hasil di area info
//     tempatHasil.innerHTML = hasil;

// });

// // Semut
// const pSemut = document.querySelector('.Semut');
// pSemut.addEventListener('click', function () {
//     // Ambil value pilihan computer
//     const pComp = getPilihanComputer();

//     // Ambil value pilihan player
//     const pPlayer = pSemut.className;

//     // ganti img comp
//     imgComp.setAttribute('src', 'img/' + pComp + '.png');

//     // Hasil
//     const hasil = getHasil(pComp, pPlayer);

//     // Tampilkan hasil di area info
//     tempatHasil.innerHTML = hasil;

// });





































// // Jika ingin bermain lagi
// var lagi;
// do {
//     // pilihan player
//     var player = prompt('Pilih Salah satu :  \n Gajah/Semut/Orang');
//     String.prototype.capitalize = function () {
//         return this.charAt(0).toUpperCase() + this.slice(1);
//     }

//     if (player == null) {

//     } else {
//         player = player.capitalize();
//     }



//     // Pilihan computer
//     // Membangkitkan bilangan random untuk computer
//     var comp = Math.random();

//     if (comp < 0.34) {
//         comp = 'Gajah';
//     } else if (comp > 0.34 && comp < 0.67) {
//         comp = 'Semut';
//     } else {
//         comp = 'Orang';
//     }

//     // Rules
//     var hasil = '';
//     if (player == comp) {
//         hasil = 'SERI!!';
//     } else if (player == 'Gajah') {
//         hasil = (comp == 'Orang') ? 'Menang' : 'Kalah';
//     } else if (player == 'Semut') {
//         hasil = (comp == 'Gajah') ? 'Menang' : 'Kalah';
//     } else if (player == 'Orang') {
//         hasil = (comp == 'Semut') ? 'Menang' : 'Kalah';
//     } else {
//         hasil = 'Tidak Valid';
//     }

//     //Tampilkan Hasil
//     if (player == null) {
//         alert('Anda tidak memilih apapun. \nMaka hasilnya ' + hasil);
//     } else if (player == '') {
//         alert('Anda tidak memilih apapun. \nMaka hasilnya ' + hasil);
//     } else {
//         alert('Kamu memilih ' + player + ' & computer memilih ' + comp + '\nMaka hasilnya ' + hasil);
//     }

//     // Bermain kembali ?
//     lagi = confirm('Ingin Bermain lagi ?');
// }
// while (lagi);


// alert('Terimakasih Sudah bermain');