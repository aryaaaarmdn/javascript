// DOM Selection

// getElementById()
// noderoot.getElementById() = document.getElementById() -> mengembalikan element
const judul = document.getElementById('judul'); // menangkap element h1 yang id nya judul
judul.style.color = 'red'; // manipulasi elemen h1 dengan mengubah warna teks

judul.style.backgroundColor = '#ccc'; //manipulasi elemen h1 dengan mengubah warna background

judul.innerHTML = "Aryaaarmdn"; // mengubah tulisan teks pada elemen h1




// getElementsByTagName()
// document.getElementsByTagName() -> mengembalikan HTMLCollecttion
const paragraf = document.getElementsByTagName('p');
// paragraf[2].style.backgroundColor = 'lightblue'; // mengubah warna background pada tag p yang berada pada index ke 2
for (var i = 0; i < paragraf.length; i++) {
    paragraf[i].style.backgroundColor = 'lightblue';
} // mengubah warna background pada semua tag p menggunakan looping 

const h1 = document.getElementsByTagName('h1'); // HTMLCollection
// console.log('Jika ingin merubah ke elemen, tambahkan index keberapa [x]');
// console.log(h1[0]);



// document.getElementsByClassName() -> mengembalikan HTMLCollection
const p1 = document.getElementsByClassName('p1')[0];
p1.innerHTML = "Ini Diubah dari javascript";