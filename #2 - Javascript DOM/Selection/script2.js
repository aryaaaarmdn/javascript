// DOM Selection
// document.getElementById()
// document.getElementsByTagName()
// document.getElementsByClassName()


// document.querySelector() -> mengembalikan element
// selector yang dimaksud disini adalah selector css
// const p4 = document.querySelector('div#container section#b p');
// p4.style.color = 'green';
// p4.style.fontSize = '30px';

const list2 = document.querySelector('div#container section#b ul li:nth-child(2)');
list2.style.backgroundColor = 'orange';

// const p = document.querySelector('p'); // maka akan dikembalikan adalah elemen yang pertama saja
// p.innerHTML = 'Ini diubah Melalui js';




// document.querySelectorAll() -> mengembalikan nodeList
// const p = document.querySelectorAll('p');
// for (let i = 0; i < p.length; i++) {
//     p[i].style.backgroundColor = 'lightblue';
// }



// mengubah node root
const sectionB = document.getElementById('b');
const p4 = sectionB.querySelector('p');
p4.style.backgroundColor = 'orange'