// Event Handler
// Inline html attribute
// const p3 = document.querySelector('.p3');

// function ubahWarna() {
//     p3.style.backgroundColor = 'lightblue';
// }

// panggil function pada inline html dengan keyword onclick



// element method
// function ubahWarna2() {
//     p2.style.backgroundColor = 'lightblue';
// }
// const p2 = document.querySelector('.p2');
// p2.onclick = ubahWarna2;




// addEventListener()
// contoh kasus : ketika paragraf 4 di klik maka akan menambahkan list baru

// const p4 = document.querySelector('section#b p');
// p4.addEventListener('click', function () {
//     const ul = document.querySelector('section#b ul');
//     const liBaru = document.createElement('li');
//     const teksLiBaru = document.createTextNode('List Baru');
//     liBaru.appendChild(teksLiBaru);
//     ul.appendChild(liBaru);
// });





// Perbedaan antara event handler dengan addEventListener()
// Event Handler
// const p3 = document.querySelector('.p3');
// p3.onclick = function () {
//     p3.style.backgroundColor = 'lightblue';
// }

// p3.onclick = function () {
//     p3.style.color = 'red';
// }
// Maka akan menimpa perubahan yang merubah warna background



// addEventListener()
const p3 = document.querySelector('.p3');
p3.addEventListener('mouseenter', function () { // ketika pointer mouse masuk ke area p3
    p3.style.backgroundColor = 'lightblue';
})
p3.addEventListener('mouseleave', function () { // ketika pointer mouse keluar dari area p3
    p3.style.color = 'red';
})
// Maka akan menambahkan event baru, tidak menimpa event sebelumnya