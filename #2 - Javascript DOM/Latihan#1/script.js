// Ubah Warna Fav
// const tombolFav = document.querySelector('#fav');
// tombolFav.addEventListener('click', function () {
//     document.body.style.backgroundColor = 'purple';
// });

// Ubah Warna Ke semula
// const tombolSemula = document.querySelector('#semula');
// tombolSemula.addEventListener('click', function () {
//     document.body.style.backgroundColor = 'white';
// });


// Class Toggle
const tombolFav = document.getElementById('fav');
tombolFav.addEventListener('click', function () {
    if (document.body.hasAttribute('style')) { // mengecek apakah body memiliki atribut style atau tidak
        document.body.removeAttribute('style');
    }

    document.body.classList.toggle('ungu');
})

// Event Warna Random
const tombolRandom = document.getElementById('random');
tombolRandom.addEventListener('click', function () {
    const r = Math.round(Math.random() * 255 + 1);
    const g = Math.round(Math.random() * 255 + 1);
    const b = Math.round(Math.random() * 255 + 1);

    document.body.style.backgroundColor = 'rgb(' + r + ',' + g + ',' + b + ')';

});

// Warna Pada slider
const slider = document.getElementById('slider');
const sMerah = document.querySelector('input[name=sliderMerah]');
const sHijau = slider.querySelector('input[name=sliderHijau]');
const sBiru = slider.querySelector('input[name=sliderBiru]');

slider.addEventListener('input', function () {
    const r = sMerah.value;
    const g = sHijau.value;
    const b = sBiru.value;

    document.body.style.backgroundColor = `rgb(${r},${g},${b})`;
});


// Mouse Move Event = perpindahan mouse
document.body.addEventListener('mousemove', function (event) {

    const xPos = Math.round((event.clientX / window.innerWidth) * 255); // Sumbu X (Atas Bawah) - untuk kadar merah
    const yPos = Math.round((event.clientY / window.innerHeight) * 255); // Sumbu Y (Kiri Kanan) - untuk kadar hijau

    // untuk kadar biru , menggunakan nilai tetap

    document.body.style.backgroundColor = 'rgb(' + xPos + ',' + yPos + ', 100)';

});