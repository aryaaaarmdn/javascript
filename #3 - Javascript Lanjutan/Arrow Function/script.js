// Function Expression
// const tampilNama = function (nama) {
//     return `Halo ${nama}`;
// }
// console.log(tampilNama('Aryaa'));

// Merubah menjadi arrow function dgn beberapa cara 
// cara 1 - 1 parameter = tanda kurung boleh dihilangkan
// const tampilNama = nama => `Halo, ${nama}`; // implisit return
// console.log(tampilNama('Aryaa'));

// cara kedua - 2 parameter atau lebih
// const tampilNama = (nama, waktu) => `Halo ${nama}, selamat ${waktu}`;
// console.log(tampilNama('aryaa', 'siang'));

// cara ketiga - tanpa parameter = tanda kurung harus di ketik
// const tampilNama = () => `Hello World`;
// console.log(tampilNama());




// let mahasiswa = ['Aryaa', 'haydari', 'Ramadhan'];
// // dengan function biasa
// let jmlHuruf = mahasiswa.map(function (nama) {
//     return nama.length;
// });

// console.log(jmlHuruf);
// // dengan arrow function
// let jmlHuruf2 = mahasiswa.map(nama => nama.length);
// console.log(jmlHuruf2);

// // mengembalikan dengan bentuk object
// let jmlHuruf3 = mahasiswa.map(nama => ({
//     nama, // di js yang baru kalo mengembalikan object yang properti sama dengan nilainya, gaperlu tulis duaduanya, cukup 1
//     jmlHuruf: nama.length
// }));
// console.log(jmlHuruf3);
// console.table(jmlHuruf3);




// Konsep this pada arrow function

// Constructor Function 
// const Mahasiswa = function () {
//     this.nama = 'Aryaa';
//     this.umur = 20;
//     console.log(this); // konteks this mengacu ke object Mahasiswa

//     this.sayHello = function () {
//         console.log(`Halo, nama saya ${this.nama}, saya berumur ${this.umur} tahun`);
//     }
// }

// let arya = new Mahasiswa();
// arya.sayHello();



// function expression tidak kena hoisting karena disimpan didalam variabel


// Arrow Function = konsep this tidak ada pada arrow function
// const Mahasiswa = function () {
//     this.nama = 'Aryaa';
//     this.umur = 20;
//     console.log(this); // konteks this mengacu ke object Mahasiswa

//     this.sayHello = () => {
//         console.log(this); // konteks this mengacu ke object Mahasiswa
//         console.log(`Halo, nama saya ${this.nama}, saya berumur ${this.umur} tahun`);
//         // konteks this pada arrow function didalam method constructor function mengacu pada object itu sendiri
//     }
// }

// let arya = new Mahasiswa();
// arya.sayHello();





// Object literal
// var nama = 'Test';
// const mhs1 = {
//     nama: 'Aryaa',
//     umur: 20,

//     sayHello: () => {
//         console.log(this); // akan menghasilkan object window (global)
//         console.log(`Halo, nama saya ${this.nama}, saya berumur ${this.umur} tahun`);
//         // Konsep this pada object literal mengacu pada global sehingga ketika menggunakan this pada arrow function pada method object literal maka akan menghasilkan nilai undefined jika tidak ada variabel yang di set di global
//     }
// }

// mhs1.sayHello();






// const Mahasiswa = function () {
//     this.nama = 'Aryaa';
//     this.umur = 20;
//     console.log(this); // konteks this mengacu ke object Mahasiswa

//     this.sayHello = function () {
//         console.log(`Halo, nama saya ${this.nama}, saya berumur ${this.umur} tahun`);
//     }

//     // setInterval(function () { // function declaration, kena hoisting maka konteks this mengacu pada global
//     //     console.log(`${this.umur++}`); // maka tidak akan menemukan variabel umur jika di global tidak ada
//     //     console.log(this); // mengacu kepada object window(global)
//     // }, 500);

//     // Gunakan arrow function pada setInterval jika ingin this mengacu pada object Mahasiswa
//     setInterval(() => {
//         // console.log(this); // karena tidak punya konsep this maka this nya akan mencari ke lexical scopenya
//         console.log(this.umur++);
//     }, 500);
// }

// let arya = new Mahasiswa();
// arya.sayHello();



const box = document.querySelector('.box');
box.addEventListener('click', function () {
    // console.log(this);
    let satu = 'size';
    let dua = 'caption';

    if (this.classList.contains(satu)) {
        [satu, dua] = [dua, satu];
    }

    this.classList.toggle(satu);

    setTimeout(() => {
        // console.log(this);
        this.classList.toggle(dua);
    }, 600)
})