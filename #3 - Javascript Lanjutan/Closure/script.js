// Execution Context, hoisting, scope

// console.log(nama);
// var nama = 'Aryaa';
// creation phase pada konteks global
// nama variabel = undefined
// nama function = fn() 
// hoisting
// window = global object
// this = window

// execution phase
// console.log(nama); = undefined
// nama = 'aryaa';



// console.log(sayHello); // creation phase pada function (hoisting isi dari fn)
// var nama = 'aryaa';
// var umur = 20;

// function sayHello() {
//     `Halo nama saya ${nama}, saya berumur ${umur} tahun`;
// }

// function membuat local execution context
// yang didalamnya terdapat creation dan execution phase
// akses window, bisa akses ke arguments
// hoisting lokal





// var nama = 'Aryaa';
// var username = '@aryaaaarmdn';

// function cetakURL() {
//     console.log(arguments);
//     var instagramURL = 'http://instagram.com/';
//     return instagramURL + username;
// }

// console.log(cetakURL('@aming'));











// function a() {
//     console.log('ini a');

//     function b() {
//         console.log('ini b');

//         function c() {
//             console.log('ini c');
//         }
//         c();
//     }
//     b()
// }
// a();

function satu() {
    var nama = 'Aryaa';
    console.log(nama);
}

function dua() {
    console.log(nama);
}

console.log(nama); // undefined

var nama = 'aming';
satu() // aryaa
dua('cuyang'); // aming
console.log(nama); // aming