// function init() {
//     // let nama = 'aryaa';
//     let umur = 20;

//     return function tampilNama(nama) {
//         console.log(nama);
//         // console.log(umur);
//     }
//     // tampilNama();
//     // return tampilNama;
// }

// let panggilNama = init(); // baru jalan sebagian , karena return tampilNama belum dijalankan
// panggilNama('Aryaa');
// panggilNama('Siapa aja');




// function factories
// function ucapkanSalam(waktu) {
//     return function (nama) {
//         console.log(`Halo ${nama}, selamat ${waktu}`);
//     }
// }

// let selamatPagi = ucapkanSalam('pagi');
// selamatPagi('Aryaa');
// let selamatSiang = ucapkanSalam('siang');
// selamatSiang('Aryaa');
// let selamatMalam = ucapkanSalam('malam');
// selamatMalam('Aryaa');

// console.dir(selamatMalam);




// Private method

let add = (function () { //IIFE
    let counter = 0; // private
    return function () {
        return ++counter;
    }
})();

counter = 100; // tidak akan mengganggu isi counter yang ada didalam function

console.log(add());
console.log(add());
console.log(add());