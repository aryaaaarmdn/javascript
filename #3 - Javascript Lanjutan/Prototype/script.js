// Cara membuat object
// 1. Object Literal
// Problem = tidak efektif untuk object yang banyak. memakan banyak resource karena melakukan sesuatu yang sama berulang ulang
// let mahasiswa = {
//     nama: 'Aryaa Ramadhan',
//     energi: 10,

//     makan: function (porsi) {
//         this.energi += porsi;
//         console.log(`Halo ${this.nama}, selamat makan!`);
//         // return `Halo ${this.nama}, selamat makan!`;
//     }
// }

// let mahasiswa2 = {
//     nama: 'Aming',
//     energi: 12,

//     makan: function (porsi) {
//         this.energi += porsi;
//         console.log(`Halo ${this.nama}, selamat makan!`);
//         // return `Halo ${this.nama}, selamat makan!`;
//     }
// }


// 2. Function Declaration

// const methodMahasiswa = {
//     makan: function (porsi) {
//         this.energi += porsi;
//         console.log(`Halo ${this.nama}, selamat makan!!`);
//     },

//     main: function (jam) {
//         this.energi -= jam;
//         console.log(`Halo ${this.nama}, selamat main !!`);
//     },

//     tidur: function (jam) {
//         this.energi += jam * 2;
//         console.log(`Halo ${this.nama}, selamat tiduur!!`);
//     }
// }

// function Mahasiswa(nama, energi) {

//     // let mahasiswa = {};
//     let mahasiswa = Object.create(methodMahasiswa); // hampir mirip dengan konsep inheritance pada konsep OOP
//     mahasiswa.nama = nama;
//     mahasiswa.energi = energi;

//     return mahasiswa;
// }

// let aryaa = Mahasiswa('Aryaa', 10);
// let aming = Mahasiswa('Aming', 12);




// 3. Constructor Function

// function Mahasiswa(nama, energi) {

//     this.nama = nama;
//     this.energi = energi;

//     this.makan = function (porsi) {
//         this.energi += porsi;
//         console.log(`Halo ${this.nama}, selamat makan!!`);
//     }

//     this.main = function (jam) {
//         this.energi -= jam;
//         console.log(`Halo ${this.nama}, selamat main !!`);
//     }
// }

// let arya = new Mahasiswa('Aryaarmdn', 10);




// function Mahasiswa(nama, energi) {

//     // let mahasiswa = Object.create(methodMahasiswa); // hampir mirip dengan konsep inheritance pada konsep OOP
//     //let this = {} // Dibelakang layar js membuatkan variabel this dengan tipe object, tidak harus di lakukan
//     // let this = Object.create(Mahasiswa.prototype); // sebetulnya ada properti yang default dijadikan parent oleh js, walaupun gaperlu ditulis

//     this.nama = nama;
//     this.energi = energi;

//     // return this
// }

// Mahasiswa.prototype.makan = function (porsi) {
//     this.energi += porsi;
//     return `Halo ${this.nama}, selamat makan!!`;
// }

// Mahasiswa.prototype.main = function (jam) {
//     this.energi -= jam;
//     return `Halo ${this.nama}, selamat main!!`;
// }
// Mahasiswa.prototype.tidur = function (jam) {
//     this.energi += jam * 2
//     return `Halo ${this.nama}, selamat tidur!!`;
// }

// let arya = new Mahasiswa('Aryaarmdn', 10);



// versi class == prototype
class Mahasiswa {
    constructor(nama, energi) {
        this.nama = nama;
        this.energi = energi;
    }

    makan(porsi) {
        this.energi += porsi;
        return `Halo ${this.nama}, selamat makan!!`;
    }
    main(jam) {
        this.energi -= jam;
        return `Halo ${this.nama}, selamat main!!`;
    }
    tidur(jam) {
        this.energi += jam * 2;
        return `Halo ${this.nama}, selamat tidur!!`;
    }
}

let arya = new Mahasiswa('Aryaa', 10);