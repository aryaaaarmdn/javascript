// kerjakanTugas = Higher Order Function ; parameter kedua(funct) yang merupakan function disebut callback
function kerjakanTugas(matakuliah, funct) {
    console.log(`Mulai mengerjakan tugas ${matakuliah}...`);
    funct();
}

function selesai() {
    alert('Selesai');
}
// kerjakanTugas('Web Programming', selesai);


// Higher order function pada beberapa method yang ada pada array
const angka = [-1, 8, 9, 1, 4, -5, -4, 3, 2, 9];

// filter(): mencari angka yang lebih besar sama dengan 3
// for (let i = 0; i < angka.length; i++) {
//     if (angka[i] >= 3) {
//         console.log(angka[i]);
//     }
// }

// const newAngka = angka.filter(angka => angka >= 3);
// console.log(newAngka);


// map() : kalikan semua angka dengan 2
// const newAngka = angka.map(angka => angka * 2);
// console.log(newAngka);
// console.log(typeof (newAngka));



// reduce = melakukan sesuatu terhadap seluruh elemen array
// jumlahkan semua isi elemen pada array
// const newAngka = angka.reduce((accumulator, currentValue) => accumulator + currentValue);
// console.log(newAngka);
// console.log(typeof (newAngka));


// method chaining = menggabungkan fungsi fungsi pada higher order function
// cari angka > 5
// kalikan 3
// jumlahkan
const hasil = angka.filter(angka => angka > 5) // 8, 9, 9
    .map(angka => angka * 3) // 24, 27, 27
    .reduce((acc, currVal) => acc + currVal); // 78

console.log(hasil); // 78