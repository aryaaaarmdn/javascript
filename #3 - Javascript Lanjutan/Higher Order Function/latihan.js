// ambil semua elemen video
const videos = Array.from(document.querySelectorAll('[data-duration]')); // nodelist bukan array

// pilih hanya yang js lanjutan
let hasil = videos.filter(hasil => hasil.textContent.includes('JAVASCRIPT LANJUTAN'))

    // ambil durasi masing masing video
    .map(item => item.dataset.duration)

    // ubah durasi menjadi float, ubah menit menjadi detik
    .map(waktu => {
        // 10:30, pecah menjadi array berdasarkan : (split) -> [10, 30]
        const parts = waktu.split(':').map(part => parseFloat(part)); // petakan setiap elemen pada array menjadi float
        return (parts[0] * 60) + parts[1]
    })

    // jumlahkan keseluruhan
    .reduce((acc, currVal) => acc + currVal);


// ubah format menjadi jam menit detik
const jam = Math.floor(hasil / 3600);
hasil = hasil - jam * 3600; // ambil menit dari sisa jam 

const menit = Math.floor(hasil / 60);
const detik = hasil - menit * 60;



// simpan di dom
const totalDurasi = document.querySelector('p .total-durasi');
totalDurasi.textContent = ` ${jam} jam, ${menit} menit, ${detik} detik.`;

const jmlVideo = videos.filter(hasil => hasil.textContent.includes('JAVASCRIPT LANJUTAN')).length; // ambil nilai total video

const videoTemplate = document.querySelector('p .jumlah-video');
videoTemplate.textContent = ` ${jmlVideo} video.`;