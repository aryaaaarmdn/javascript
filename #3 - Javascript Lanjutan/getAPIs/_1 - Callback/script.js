function getAjax() {
    $.ajax({
        url: 'http://www.omdbapi.com/?apikey=7b8ac6ea&s=' + $(".input-keyword").val(),
        success: result => {
            const movies = result.Search;
            // console.log(movies);
            let el = ''

            movies.forEach(movie => {
                el += showCards(movie);
            })

            $('.row:nth-of-type(2)').html(el);

            // ketika tombol detail di klik
            $(".modal-Detail-Button").on('click', function () {
                // console.log($(this).data('imdbid'));
                $.ajax({
                    url: 'http://www.omdbapi.com/?apikey=7b8ac6ea&i=' + $(this).data('imdbid'),
                    success: result => {
                        // console.log(result);
                        const detail = showMovieDetail(result);
                        $(".modal-body").html(detail);
                    },
                    error: (e) => {
                        console.log(e.responseText);
                    }
                })
            })
        },
        error: (e) => {
            console.log(e.responseText);
        }
    })
}


$(".search-button").on('click', function () {
    getAjax();
})

$(".input-keyword").on('keyup', function () {
    if (event.which === 13) {
        getAjax();
    }
})



function showCards(movie) {
    return `<div class="col-md-4 my-3">
                <div class="card">
                    <img src="${movie.Poster}" class="card-img-top">
                    <div class="card-body">
                        <h5 class="card-title">${movie.Title}</h5>
                        <h6 class="card-subtitle mb-2 text-muted">${movie.Year}</h6>
                        <a href="#" class="btn btn-primary modal-Detail-Button" data-toggle="modal" data-target=".bd-example-modal-lg" data-imdbid="${movie.imdbID}">Detail</a>
                    </div>
                </div>
            </div>`;
}

function showMovieDetail(result) {
    return `<div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <img src="${result.Poster}" class="img-fluid">
                    </div>

                    <div class="col-md">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <h4>${result.Title} (${result.Year})</h4>
                            </li>
                            <li class="list-group-item">
                                <strong>Director : </strong>
                                ${result.Director}
                            </li>
                            <li class="list-group-item">
                                <strong>Actors : </strong>
                                ${result.Actors}
                            </li>
                            <li class="list-group-item">
                                <strong>Writer : </strong>
                                ${result.Writer}
                            </li>
                            <li class="list-group-item">
                                <strong>Plot : </strong>
                                ${result.Plot}
                            </li>
                        </ul>
                    </div>
                </div>
            </div>`;
}