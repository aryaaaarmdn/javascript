const tombolCari = document.querySelector(".search-button");
tombolCari.addEventListener('click', async function () { // async = memberitahu bahwa isi function akan ada yg asynchronous
    const keyword = document.querySelector(".input-keyword");

    const movies = await getMovies(keyword.value); // await = memberitahu proses yang async
    updateUI(movies);
})

// Ketika tombol detail di klik
// event binding = memberikan event pada elemen yang awalnya belum ada tp ketika elemen ada, event nya masih bisa berjalan
document.addEventListener('click', async function (e) {
    if (e.target.classList.contains('modal-Detail-Button')) {
        const imdbid = e.target.dataset.imdbid;
        const movieDetail = await getMovieDetail(imdbid);
        updateUIDetail(movieDetail);
    }
})

function getMovieDetail(imdbid) {
    return fetch("http://www.omdbapi.com/?apikey=7b8ac6ea&i=" + imdbid)
        .then(response => response.json())
        .then(result => result);
}

function updateUIDetail(m) {
    const detail = showMovieDetail(m);
    const modalBody = document.querySelector('.modal-body');
    modalBody.innerHTML = detail;
}


function getMovies(keyword) {
    return fetch("http://www.omdbapi.com/?apikey=7b8ac6ea&s=" + keyword)
        .then(response => response.json())
        .then(response => response.Search);
}

function updateUI(movies) {
    let cards = '';

    movies.forEach(m => cards += showCards(m));

    const movieContainer = document.querySelector('.movie-container');
    movieContainer.innerHTML = cards;
}

function showCards(movie) {
    return `<div class="col-md-4 my-3">
                <div class="card">
                    <img src="${movie.Poster}" class="card-img-top">
                    <div class="card-body">
                        <h5 class="card-title">${movie.Title}</h5>
                        <h6 class="card-subtitle mb-2 text-muted">${movie.Year}</h6>
                        <a href="#" class="btn btn-primary modal-Detail-Button" data-toggle="modal" data-target=".bd-example-modal-lg" data-imdbid="${movie.imdbID}">Detail</a>
                    </div>
                </div>
            </div>`;
}

function showMovieDetail(result) {
    return `<div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <img src="${result.Poster}" class="img-fluid">
                    </div>

                    <div class="col-md">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <h4>${result.Title} (${result.Year})</h4>
                            </li>
                            <li class="list-group-item">
                                <strong>Director : </strong>
                                ${result.Director}
                            </li>
                            <li class="list-group-item">
                                <strong>Actors : </strong>
                                ${result.Actors}
                            </li>
                            <li class="list-group-item">
                                <strong>Writer : </strong>
                                ${result.Writer}
                            </li>
                            <li class="list-group-item">
                                <strong>Plot : </strong>
                                ${result.Plot}
                            </li>
                        </ul>
                    </div>
                </div>
            </div>`;
}