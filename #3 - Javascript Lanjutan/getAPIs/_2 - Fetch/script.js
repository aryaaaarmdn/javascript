const tombolCari = document.querySelector(".search-button");

tombolCari.addEventListener('click', function () {
    const keyword = document.querySelector(".input-keyword");

    fetch("http://www.omdbapi.com/?apikey=7b8ac6ea&s=" + keyword.value)
        .then(response => response.json())
        .then(response => {

            const movies = response.Search;
            let cards = '';

            movies.forEach(m => cards += showCards(m));

            const movieContainer = document.querySelector('.movie-container');
            movieContainer.innerHTML = cards;

            const modalButton = document.querySelectorAll('.modal-Detail-Button');
            modalButton.forEach(tombol => {
                tombol.addEventListener('click', function () {
                    fetch("http://www.omdbapi.com/?apikey=7b8ac6ea&i=" + this.dataset.imdbid)
                        .then(response => response.json())
                        .then(result => {
                            const detail = showMovieDetail(result);
                            const modalBody = document.querySelector('.modal-body');
                            modalBody.innerHTML = detail;
                        });
                })
            });

        })
})

function showCards(movie) {
    return `<div class="col-md-4 my-3">
                <div class="card">
                    <img src="${movie.Poster}" class="card-img-top">
                    <div class="card-body">
                        <h5 class="card-title">${movie.Title}</h5>
                        <h6 class="card-subtitle mb-2 text-muted">${movie.Year}</h6>
                        <a href="#" class="btn btn-primary modal-Detail-Button" data-toggle="modal" data-target=".bd-example-modal-lg" data-imdbid="${movie.imdbID}">Detail</a>
                    </div>
                </div>
            </div>`;
}

function showMovieDetail(result) {
    return `<div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <img src="${result.Poster}" class="img-fluid">
                    </div>

                    <div class="col-md">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <h4>${result.Title} (${result.Year})</h4>
                            </li>
                            <li class="list-group-item">
                                <strong>Director : </strong>
                                ${result.Director}
                            </li>
                            <li class="list-group-item">
                                <strong>Actors : </strong>
                                ${result.Actors}
                            </li>
                            <li class="list-group-item">
                                <strong>Writer : </strong>
                                ${result.Writer}
                            </li>
                            <li class="list-group-item">
                                <strong>Plot : </strong>
                                ${result.Plot}
                            </li>
                        </ul>
                    </div>
                </div>
            </div>`;
}