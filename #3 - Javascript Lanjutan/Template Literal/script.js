// Multi line string
// mengggunakan ''
console.log('Multi line string tanpa template literal\nstring 1 \nstring 2');
// menggunakan ``
console.log(`Multi line string menggunakan template literal
String 1
String 2`);


// HTML Fragments
const mhs = {
    nama: 'Aryaaarmdn',
    umur: 20,
    nim: 1803015133,
    email: 'aryaramadhan097@gmail.com'
};

let elemen = '';
elemen += 'div class="mhs">';
elemen += '<h2>' + mhs.nama + '</h2>';
elemen += '<span class="nim">' + mhs.nim + '</span>';
elemen += '</div>';

console.log('HTML Fragments tanpa template literal\n' + elemen);

// HTML Fragments menggunakan template literal
let elemen2 = `<div class="mhs"
    <h2>${mhs.nama}</h2>
    <span class="nim">${mhs.nim}</span>
</div>`;

console.log(`HTML Fragment dengan template literal
${elemen2}`);


// embedded expression
const nama = 'aryaa'
let umur = 20;
console.log(`Embedded Expression : 
Halo nama saya ${nama}
Saya berumur ${umur} tahun`);

console.log(`Embedded Expression 
1 + 1 = ${1 + 1}`);

// console.log(`Embedded Expression memanggil function alert ${alert('halo')}`); // embedded expression memanggil function

const x = 11;
// embedded expression ternary
console.log(`Embedded Expression operasi ternary
${(x % 2 == 0 ? 'Genap' : 'Ganjil')}`);



// embedded expression & interpolasi

let a = 10,
    b = 15;

console.log('Interpolasi tanpa template literal\nJika a = 10 dan b = 15\nMaka hasil penjumlahannya adalah ' +
    (a + b) + ' bukan ' + (2 * a + b));
console.log(`Interpolasi menggunakan template literal
jika a = 10 dan b = 15
maka hasil penjumlahannya adalah : ${a + b}. bukan ${2*a+b}`);