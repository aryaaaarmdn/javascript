// Jika ingin bermain lagi
var lagi;
do {
    // pilihan player
    var player = prompt('Pilih Salah satu :  \n Gajah/Semut/Orang');
    String.prototype.capitalize = function () {
        return this.charAt(0).toUpperCase() + this.slice(1);
    }
    if (player == null) {

    } else {
        player = player.capitalize();
    }

    // Pilihan computer
    // Membangkitkan bilangan random untuk computer
    var comp = Math.random();

    if (comp < 0.34) {
        comp = 'Gajah';
    } else if (comp > 0.34 && comp < 0.67) {
        comp = 'Semut';
    } else {
        comp = 'Orang';
    }

    // Rules
    var hasil = '';
    if (player == comp) {
        hasil = 'SERI!!';
    } else if (player == 'Gajah') {
        hasil = (comp == 'Orang') ? 'Menang' : 'Kalah';
    } else if (player == 'Semut') {
        hasil = (comp == 'Gajah') ? 'Menang' : 'Kalah';
    } else if (player == 'Orang') {
        hasil = (comp == 'Semut') ? 'Menang' : 'Kalah';
    } else {
        hasil = 'Tidak Valid';
    }

    //Tampilkan Hasil
    if (player == null) {
        alert('Anda tidak memilih apapun. \nMaka hasilnya ' + hasil);
    } else if (player == '') {
        alert('Anda tidak memilih apapun. \nMaka hasilnya ' + hasil);
    } else {
        alert('Kamu memilih ' + player + ' & computer memilih ' + comp + '\nMaka hasilnya ' + hasil);
    }

    // Bermain kembali ?
    lagi = confirm('Ingin Bermain lagi ?');
}
while (lagi);


alert('Terimakasih Sudah bermain');