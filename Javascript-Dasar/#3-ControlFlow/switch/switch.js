// var angka = prompt('masukkan angka ; ');

// if (angka == 1) {
//     alert('anda memasukkan angka 1');
// } else if (angka == 2) {
//     alert('anda memasukkan angka 2');
// } else if (angka == 3) {
//     alert('anda memasukkan angka 3');
// } else {
//     alert('anda memasukkan angka yang salah');
// }

// switch (angka) {
//     case '1':
//         alert('Anda memasukkan angka 1');
//         break;
//     case '2':
//         alert('Anda memasukkan angka 2');
//         break;
//     case '3':
//         alert('Anda memasukkan angka 3');
//         break;
//     default:
//         alert('anda memasukkan angka yang salah');
//         break;
// }


var item = prompt('Masukkan nama makanan / minuman : \n (contoh) nasi, daging, susu, hamburger, softdrink');

// switch (item) {
//     case 'nasi':
//         alert('Makanan/minuman yang sehat');
//         break;
//     case 'daging':
//         alert('Makanan/minuman yang sehat');
//         break;
//     case 'susu':
//         alert('Makanan/minuman yang sehat');
//         break;
//     case 'hamburger':
//         alert('Makanan/minuman yang tidak sehat');
//         break;
//     case 'softdrink':
//         alert('Makanan/minuman yang tidak sehat');
//         break;
//     default:
//         alert('makanan/minuman tidak tersedia pada contoh');
//         break;
// }

// perbaikan 
switch (item) {
    case 'nasi':
    case 'daging':
    case 'susu':
        alert('Makanan/minuman yang sehat');
        break;
    case 'hamburger':
    case 'softdrink':
        alert('Makanan/minuman yang tidak sehat');
        break;
    default:
        alert('makanan/minuman tidak tersedia pada contoh');
        break;
}