function tambah(a, b) { // a dan b adalah parameter yang akan menerima nilai dari argumen ketika function dipanggil
    return a + b;
}

// memasukkan nilai langsung ke argument
console.log('hasil menggunakan nilai langsung ke argumen (2, 3) adalah ' + tambah(2, 3)); // 2 dan 3 adalah argumen yang akan dikirimkan ke parameter a dan b

// memasukkan nilai argumen menggunakan variabel yang sudah ditetapkan nilainya

var nilai1 = 5;
var nilai2 = 3;
console.log('hasil menggunakan variabel (variabel nilai 1 = 5 & variabel nilai 2 = 3) adalah ' + tambah(nilai1, nilai2));

// memasukkan nilai argumen melalui inputan user
var nilai3 = parseInt(prompt('Masukkan nilai pertama : '));
var nilai4 = parseInt(prompt('Masukkan nilai kedua : '));
// hasil inputan dari prompt itu berupa string, jika ingin menjumlahkan sebuah bilangan, maka hasil inputan dari prompt harus di parsing dari string ke integer
console.log('Hasil menggunakan inputan user adalah ' + tambah(nilai3, nilai4));

// operasi matematik pada argumen
console.log('hasil menggunakan operasi matematika ' + tambah(nilai3 * 2, nilai4 * 2));


function kali(a, b) {
    return a * b;
}


// memasukkan argumen menggunakan function
var hasil = kali(tambah(2, 3), tambah(3, 4));
console.log(hasil); // hasilnya 35

// contoh arguments
function cobaArguments() {
    return arguments;
}

var coba = cobaArguments(1, 2, 3, "Ya", true);
console.log(coba);

// contoh kasus arguments
function tambahMenggunakanArguments() {
    var total = 0;
    for (i = 0; i < arguments.length; i++) {
        total += arguments[i];
    }
    return total;
}

var coba2 = tambahMenggunakanArguments(1, 2, 3, 4, 5, 3, 2);
console.log(coba2);