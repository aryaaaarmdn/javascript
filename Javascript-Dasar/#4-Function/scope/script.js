// variabel global / window scope = variabel yang bisa diakses dimana saja
var a = 1;

// function tes() {
//     var b = 2;
//     console.log(a); // menampilkan angka 1 karena bisa diakses dimana saja 
// }

// tes();
// console.log(b); // error karena tidak dikenali diluar function

// function tes() {
//     // name conflict = mempunyai variabel dengan nama yang sama tetapi beda scope
//     // tidak menimpa nilai pada function 
//     var a = 2;
//     // console.log(a); // karena a ini adalah variabel lokal
//     // console.log(window.a); // akan menampilkan variabel global a
// }

// tes();
// console.log(a);

// function tes() {
//     a = 2; // menimpa variabel global, karena sudah di deklarasi var pada scope global
//     // jika ada declarasi var di dalam function maka akan membuat scope lokal
// }

// tes();
// console.log(a);

function tes(a) {
    console.log(a);
}

// tes(2); // akan ditangkap oleh parameter a yang dimana itu scope lokal 
// console.log(a); // menampilkan variabel global a

tes(a); // akan menampilkan 1 karena a di argumen adalah variabel global 