// infinite rekursif
// function tes() {
//     return tes();
// }

// tes();

// infinite rekursif #2
// function tampilAngka(n) {
//     console.log(n);
//     return tampilAngka(n - 1);
// }
// tampilAngka(10);

// rekursif
function tampilAngka(n) {
    if (n === 0) {
        return; // base case
    }

    console.log(n);
    return tampilAngka(n - 1);
}

tampilAngka(10);

function faktorial(n) {
    if (n === 0) return 1; //base case

    return n * faktorial(n - 1);
}

console.log(faktorial(5));