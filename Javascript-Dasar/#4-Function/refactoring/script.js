// contoh refactoring sederhana

// sebelum di refactoring
// function jmlVolumeDuaKubus(a, b) {
//     var volumeA;
//     var volumeB;
//     var total;

//     volumeA = a * a * a;
//     volumeB = b * b * b;
//     total = volumeA + volumeB;

//     return total;
// }

// sesudah refactoring
function jmlVolumeDuaKubus(a, b) {
    return ((a * a * a) + (b * b * b));
}

console.log(jmlVolumeDuaKubus(3, 4));