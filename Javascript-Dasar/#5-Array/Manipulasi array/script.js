// Manipulasi array

// 1. Menambah isi array
var arr = [];
arr[0] = "Aryaa";
arr[1] = "Ramadhan";
arr[3] = "Puri"; // jml elemen ada 4 yang di index ke 2 akan berisi nilai undefined/empty
arr[6] = "Nuri";
console.log(arr);
console.log(arr[2]);
// empty bukan tipe. Itu hanya cara menggambarkan bahwa tidak ada apa pun di sana. Itu tidak terdefinisi atau nol. Ini sama sekali tidak memiliki nilai. Anda hanya akan melihat itu ketika mencoba menampilkan representasi array. Jika Anda benar-benar melihat indeks itu, itu akan mengembalikan undefined.

// 2. Menghapus isi array
var arr2 = ["Aryaa", "Haydari", "Ramadhan"];
arr2[1] = undefined;
console.log(arr2);

// 3. menampilkan isi array
var arr3 = ["Aryaa", "Haydari", "Ramadhan", "Puri", "Nuri"];
for (var i = 0; i < arr3.length; i++) { // Length = menghitung jml elemen pada array
    console.log("Mahasiswa Ke-" + (i + 1) + ": " +
        arr3[i]);
}


// Method pada array
// - length = menghitung jml elemen pada array
// - join = yang menggabungkan seluruh isi array dan mengubahnya menjadi string
var arr4 = ["Aryaa", "Haydari", "Ramadhan"];
console.log(arr4.join(" ")); // parameter pertama berupa separator (pemisah), defaultnya adalah - (strip)

// - push, pop, shift, unshift
// untuk menambah/menghapus elemen pada array
// (+) push = menambah elemen array di akhir array
arr4.push("Puri", "Nuri");
console.log(arr4.join());
// (+) pop = menghilangkan elemen terakhir pada array
arr4.pop();
arr4.pop();
console.log(arr4.join(" "));


// unshift & shift 
arr4.unshift("Nama", "Saya");
console.log(arr4.join(" "));
arr4.shift();
arr4.shift();
console.log(arr4.join(" "));


// Slice & Splice 
// Splice = menyambungkan / menambal , menyisipkan sebuah elemen ditengah tengah
// rumus : splice(indexAwal, mau dihapus berapa(opsional), elemenBaru1(opsional), elemenBaru2(opsional), ...)
var arr5 = ["Arya", "Haydari", "Ramadhan"];
// arr5.splice(2, 0, "Puri", "Nuri"); // menambahkan elemen baru (puri & nuri) dimulai dari index ke 2
arr5.splice(1, 2, "Puri", "Nuri") // dari index ke 1 dihapus 2 elemen lalu ditambahkan 2 elemen baru (Puri & Nuri)
console.log(arr5.join(" - "));

// Slice = mengiris sebuah array menjadi array yang baru
// rumus : slice(awal, akhir) // index awal akan terbawa di array yang baru tetapi index akhir tidak
var arr6 = ["Arya", "Haydari", "Ramadhan", "Puri", "Nuri"];
var arr7 = arr6.slice(1, 3);
var arr8 = arr6.slice(3);
console.log(arr6.join(" - "));
console.log(arr7.join(" - "));
console.log(arr8.join(" - "));


// Foreach & map
// foreach = pengulangan khusus untuk array
var angka = [1, 3, 20, 2, 40, 6, 4, 5, 7, 8];
var nama = ["Arya", "Puri", "Nuri"];
// menggunakan for
// for (var i = 0; i < angka.length; i++) {
//     console.log(angka[i]);
// }
// menggunakan foreach
angka.forEach(function (e) {
    console.log(e);
});

nama.forEach(function (e, i) {
    console.log("Mahasiswa ke-" + (i + 1) + " adalah " + e);
});



// Map = hampir sama dengan foreach, tapi map mengembalikan nilai array
var angka2 = angka.map(function (e) {
    return e * 2;
});
console.log(angka2.join(" - "));


// sort 
console.log(angka.join(" - "));
angka.sort();
console.log(angka.join(" - "));
angka.sort(function (a, b) {
    return a - b;
});
console.log(angka.join(" - "));


// filter & find = untuk mencari elemen pada array
// find = mengembalikan 1 nilai
var angka4 = angka.find(function (x) {
    return x > 5;
});
console.log(angka4);
// filter = menghasilkan banyak nilai
var angka3 = angka.filter(function (x) {
    // return x == 5;
    // return x == 11;
    return x > 5;
});

console.log(angka3.join(" - "));