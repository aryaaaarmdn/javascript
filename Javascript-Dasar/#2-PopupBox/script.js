// Contoh alert
alert('Ini adalah contoh alert');
// alert('halo');
// alert('nama');
// alert('saya');
// alert('Arya');

// Contoh Prompt
// prompt('Masukkan nama : ');
// Menampilkan isi dari inputan pada prompt
var nama = prompt('Ini Adalah contoh pop up prompt, Masukkan nama :')
alert('Nama anda adalah ' + nama);

// Contoh confirm
var konfirm = confirm('Ini adalah contoh pop up confirm.. Apakah kamu yakin ?');
if (konfirm === true) {
    alert('Anda menekan tombol ok');
} else {
    alert('Anda menekan tombol no');
}