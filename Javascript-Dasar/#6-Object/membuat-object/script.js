// Membuat Object

// 1. Object Literal
var mhs = {
    nama: "Aryaa Ramadhan",
    nim: 1803015133,
    email: "aryaramadhan097@gmail.com",
    jurusan: "Teknik Informatika"
}

var mhs2 = {
    nama: "Puri Lovita Aris",
    nim: 1803015134,
    email: "puri@gmail.com",
    jurusan: "Teknik Informatika"
}



// 2. Function Declaration
function buatObjectMahasiswa(nama, nim, email, jurusan) {
    var mhs = {};
    mhs.nama = nama;
    mhs.nim = nim;
    mhs.email = email;
    mhs.jurusan = jurusan

    return mhs;
}

var mhs3 = buatObjectMahasiswa("Nuri", 1803015135, "nuri@gmail.com", "PGSD");
var mhs4 = buatObjectMahasiswa("Imam", 1803014133, "imam@yahoo.com", "Teknik Mesin")





// 3. Constructor Function
// Kebiasaan / Konvensi saat membuat function constructor biasanya dengan membuat Class (diawali huruf Besar (camel case))
function Mahasiswa(nama, nim, email, jurusan) {
    this.nama = nama;
    this.nim = nim;
    this.email = email;
    this.jurusan = jurusan;
}

var mhs5 = new Mahasiswa("Angga", 1803015136, "angga@outlook.com", "Teknik Elektro");