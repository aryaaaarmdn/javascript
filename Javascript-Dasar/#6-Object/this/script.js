// Konsep this
// console.log(this); // isi dari keyword this adalah window/object global
// console.log(window); // sama dengan this
// console.log(window === this); // true

// var a = 10;
// console.log(a);
// console.log(this.a);
// console.log(window.a);
// setiap membuat variabel global itu sama aja dia adalah properti/method dari object window
// Context pertama : kalo bikin di scope global, this adalah window 




// Membuat object

// Cara 1 - Function Declaration
// function halo() {
//     console.log(this);
//     console.log("Halo");
// }
// halo(); // sama
// window.halo(); // sama
// this.halo(); // sama
// this pada function declaration = mengembalikan object global



// Cara 2 - Object literal
var obj = {
    a: 10,
    nama: "Aryaarmdn"
}
obj.halo = function () {
    console.log(this);
    console.log("halo");
}

obj.halo(); // cara memanggil
// This pada object literal = mengembalikan object yang bersangkutan beserta isinya


// Cara 3 - Constructor Function
// function Halo() {
//     console.log(this);
//     // console.log("Halo");
// }
// var obj1 = new Halo();
// var obj2 = new Halo();
// This pada constructor function = mengembalikan object yang baru dibuat (instansiasi) (obj1 & obj2)