// Membuat object
var coba = {} // object kosong
// isi dari object bisa dimasukkan dari console (Hanya di memori, jika di refresh browser maka akan hilang)

var mhs = {
    nama: "Aryaa Ramadhan",
    umur: 20,
    IPSemester: [2.5, 2.6, 2.7],
    alamat: {
        jalan: "Jln Kayu Tinggi",
        kota: "Jakarta Timur",
        provinsi: "Jakarta"
    }
};

// mhs["nama"] atau mhs.nama 