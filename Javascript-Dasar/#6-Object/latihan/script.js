/* Contoh
Angkot 1    =>  Data awal   :   - Supir : Aryaaaarmdn
                                - Trayek : Cicaheum - Cibiru
                                - Penumpang : kosong
                                - kas : 0 */

function Angkot(supir, trayek, penumpang, kas) {
    this.supir = supir;
    this.trayek = trayek;
    this.penumpang = penumpang;
    this.kas = kas;


    this.penumpangNaik = function (namaPenumpang) {
        this.penumpang.push(namaPenumpang);
        return this.penumpang;
    }

    this.penumpangTurun = function (namaPenumpang, bayar) {
        if (this.penumpang.length == 0) {
            alert("Angkot Masih Kosong");
            return false;
        } else {
            for (var i = 0; i < this.penumpang.length; i++) {
                if (this.penumpang[i] == namaPenumpang) {
                    this.penumpang[i] = undefined;
                    this.kas += bayar;
                    return this.penumpang;
                } else if (i === this.penumpang.length - 1) {
                    console.log(namaPenumpang + " Tidak ada di angkot")
                }
            }
        }
    }
}

var angkot1 = new Angkot("Aryaaaarmdn", ["Cakung", "PuloGadung"], [], 0);
var angkot2 = new Angkot("Imam", ["Bekasi", "PuloGadung"], [], 0);