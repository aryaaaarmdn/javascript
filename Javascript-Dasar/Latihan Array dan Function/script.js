var penumpang = [];
var tambahPenumpang = function (namaPenumpang, penumpang) {
    // Jika angkot kosong
    if (penumpang.length == 0) {
        // tambah penumpang diawal array
        penumpang.unshift(namaPenumpang);
        // Kembalikan isi array dan keluar dari function
        return penumpang;

    } else { // else
        // telusuri seluruh kursi dari awal
        for (var i = 0; i < penumpang.length; i++) {
            // Jika sudah ada nama yang sama
            if (penumpang[i] == namaPenumpang) {
                // Tampilkan pesan kesalahan
                console.log(namaPenumpang + " Sudah berada didalam angkot");
                // kembalikan isi array dan keluar dari function
                return penumpang;
            }
            // Jika ada kursi kosong
            else if (penumpang[i] == undefined) {
                // Tambah Penumpang dari kursi tersebut
                penumpang[i] = namaPenumpang;
                // kembalikan isi array dan keluar dari function
                return penumpang;
            } else if (i == penumpang.length - 1) { // Jika seluruh kursi terisi
                // tambah penumpang diakhir array (kursi)
                penumpang.push(namaPenumpang);
                // kembalikan isi array dan keluar dari function
                return penumpang;
            }
        }
    }
}

var hapusPenumpang = function (namaPenumpang, penumpang) {
    // jika angkot kosong
    if (penumpang.length == 0) {
        // tampilkan pesan 
        console.log("Angkot masih kosong");
        // kembalikan isi array
        return penumpang;
    } else {
        // Telusuri seluruh kursi
        for (var i = 0; i < penumpang.length; i++) {
            // Jika nama penumpang sesuai
            if (penumpang[i] == namaPenumpang) {
                // Hapus penumpang dengan memberi nilai undefined
                penumpang[i] = undefined;
                // Kembalikan isi array
                return penumpang;
            } else if (i == penumpang.length - 1) { // Jika tidak ada nama penumpang yang sesuai diangkot
                // Tampilkan pesan
                console.log("Tidak ada " + namaPenumpang + " diangkot ini");
                // Kembalikan isi array
                return penumpang;
            }
            // return penumpang;
        }
    }
}